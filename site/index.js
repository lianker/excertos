let fields = [
    document.querySelector('#txt-title'),
    document.querySelector('#txt-content'),
];

document.querySelector('.form').addEventListener('submit', function(event) {
    //	cancelando	a	submissão	do	formulário
    event.preventDefault();
    let title = fields[0].value
    let content = fields[1].value

    let cards = document.querySelector(".cards-container")
    cards.insertBefore(createCard(title, content), cards.firstChild)

    // crearing fields
    fields.forEach(field => field.value = null)
});

function createCard(title, content) {
    let div = document.createElement("div")
    div.setAttribute("class", "card box")
    
    let h2 = document.createElement("h2")
    h2.textContent = title
    
    let cardContent = document.createElement("p")
    cardContent.textContent = content


    div.appendChild(h2)
    div.appendChild(cardContent)
    return div
}